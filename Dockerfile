FROM congyuandong/node6

MAINTAINER congyuandong <congyuandong@gmail.com>

WORKDIR /app

COPY . /app/

RUN cnpm install \
  && cnpm rebuild node-sass --force \
  && cnpm run build:prod \
  && cp -r dist/* /var/www/html \
  && rm -rf /app

CMD ["nginx","-g","daemon off;"]
