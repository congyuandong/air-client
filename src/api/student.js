import fetch from '@/utils/fetch'

const domain = 'http://api.airenglish.site'

export function fetchStudent(query) {
  return fetch({
    url: `${domain}/newbee/detail/`,
    method: 'get',
    params: query
  })
}

export function syncStudent(query) {
  const t = new Date(query.time)
  const time = `${t.getFullYear()}-${t.getMonth() + 1}`
  return fetch({
    url: `${domain}/system/statistics/`,
    method: 'get',
    params: {
      time
    }
  })
}

export function fetchConfig() {
  return fetch({
    url: `${domain}/system/config/`,
    method: 'get'
  })
}
