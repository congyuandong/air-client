'use strict'

import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const students = {
  state: {
    newbee: [],
    school: []
  },

  mutations: {
    SET_NEWBEE: (state, newbee) => {
      state.newbee = newbee
    },
    SET_SCHOOL: (state, school) => {
      state.school = school
    }
  },
  getters: {
    newbees(state) {
      return state.newbee
    },
    schools(state) {
      return state.school
    }
  },
  actions: {
    getNewbee({ commit }) {
      return new Promise((resolve) => {
        Vue.resource('http://api.airenglish.site/newbee/master').get().then((response) => {
          if (response.ok) {
            commit('SET_NEWBEE', response.body)
            resolve()
          }
        })
      })
    },
    getNewbeeBySchool({ commit }) {
      return new Promise((resolve) => {
        Vue.resource('http://api.airenglish.site/newbee/child').get().then((response) => {
          if (response.ok) {
            commit('SET_NEWBEE', response.body.newbees)
            commit('SET_SCHOOL', response.body.schools)
            resolve()
          }
        })
      })
    },
    getStudentsDetail({ commit }) {
      return new Promise((resolve) => {
        Vue.resource('http://api.airenglish.site/newbee/detail/2017-11').get().then((response) => {
          if (response.ok) {
            resolve(response.body)
          }
        })
      })
    }
  }
}

export default students
